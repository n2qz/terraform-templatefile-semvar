output "final-configuration-file" {
  value       = "${data.template_file.outputfile.rendered}"
  description = "Output of the rendred file"
}

output "match-inputs" {
  value = [
    "Requested Version: ${var.request_semver}",
    "Threshold Version: ${var.threshold_semvar}"
  ]
  description = "Helm Chart versioned used for comparison"
  sensitive   = true
}

output "is-greater-or-equal" {
  value       = "${local.is_greater_or_equal}"
  description = "Is the requested semver greater than the compared semver"
}

output "match-logic" {
  value = [
    "Major is greater than == ${local.major_major}",
    "OR",
    "Major is equal == ${local.major}",
    "Major is equal AND minor is greater-or-equal  == ${local.minor}",
    "Major is equal AND minor is equal AND patch is greater-or-equal == ${local.patch}",
    "Incoming ${var.request_semver} greater-than-equal to Threshold ${var.threshold_semvar} is ${local.is_greater_or_equal}"
  ]
  sensitive   = true
  description = "Output matrix of variables used to compare semvars"
}