# Overview

This project is intended to demonstrate how to switch a label in a template file based on the semver being greater-than-equal-to or less-than a provided semver.

:warning: Note: This is a hack for strict syntax systems where extra or absence of variables will fail validation. Tools like Helm ignore parameters that are not used, so consider adding a second variable instead of implementing a switch of labels.

## Background

This originated from a terraform module that uses a `.tpl/.tmpl` file to configure Helm charts. In version 3.0.0 of the Gitlab Helm chart, the property for `redis.enabled` changed to `redis.install`. Build tools can fail when warnings of deprecation are expressed, and this is what started this fun small hack.

### Config Files

Pre "3.0.0", the YAML file uses "redis.enabled" and after the "3.0.0" version, the YAML file uses "redis.install".  The goal of this hack is to show how to change the "redis.**enable||install** with Terraform's `template_file` function.

#### Pre "3.0.0" YAML file:
```yaml
  ...
  redis:
    enabled: false # or true
  ...
```

#### After "3.0.0" YAML file:
```yaml
  ...
  redis:
    install: false # or true
  ...
```

## Goal

Replace the "label" of the property ("enabled") with "installed" when the semver of the Helm chart is greater-or-equal to 3.0.0.

### Running Tests

1. To run the examples, install Terraform (version 11+)

1. Start with:

    ```bash
    terraform init
    ```

1. Run the below test/confirmation commands

### Default Values

| Requested Version | Threshold Version | Expected Result | Label Output     |
|:-----------------:|:-----------------:|:---------------:|:-----------------|
| 2.3.7             | 3.0.0             | false           | `enable: false`  |

```bash
terraform apply
```

### Requesting newer than 3.0.0 semver

| Requested Version | Threshold Version | Expected Result | Label Output     |
|:-----------------:|:-----------------:|:---------------:|:-----------------|
| 3.0.1             | 3.0.0             | true            | `install: false` |

```bash
terraform apply -var="request_semver=3.0.1"
```

### Requesting same-as 3.0.0 semver

| Requested Version | Threshold Version | Expected Result | Label Output     |
|:-----------------:|:-----------------:|:---------------:|:-----------------|
| 3.0.0             | 3.0.0             | true            | `install: false` |

```bash
terraform apply -var="request_semver=3.0.0"
```
### Open Form semver comparison

Set both parameters to test different versions to compare with

| Requested Version | Threshold Version | Expected Result | Label Output     |
|:-----------------:|:-----------------:|:---------------:|:-----------------|
| 2.5.1             | 2.1.0             | true            | `install: false` |

```bash
terraform apply -var="request_semver=2.5.1" -var="threshold_semvar=2.1.0"
```

### Bash Tests

A simple series of tests are available for use

```bash
./test-cases.sh
```

## Output

This example uses the `outputs` to display the rendered file.  To avoid always showing the logic, the `match-logic` and `input` outputs are set to `sensitive` so they do not display on the command line.  To view the values:

```bash
terraform apply # or as-above
terraform output match-logic
terraform output match-inputs
```