#!/bin/bash

DENOM=','

declare -A TESTS
TESTS["2.5.1${DENOM}2.1.0"]='true'
TESTS["3.0.0${DENOM}3.0.0"]='true'
TESTS["2.5.1${DENOM}3.0.0"]='false'
TESTS["0.0.0${DENOM}0.0.0"]='true'
TESTS["10.0.0${DENOM}1.0.0"]='true'
TESTS["1.10.0${DENOM}1.0.0"]='true'
TESTS["1.0.13${DENOM}1.0.14"]='false'

if [ ! -d ".terraform" ]; then
    echo "Initializing Terraform"
    terraform init > /dev/null
fi

SUMMARY=SUCCESS
for i in "${!TESTS[@]}"
do
    VALUES=($(echo $i | tr ${DENOM} " "))
    REQUEST="${VALUES[0]}"
    THRESHOLD="${VALUES[1]}"
    EXPECT="${TESTS[$i]}"

    if ( terraform apply -auto-approve -var="request_semver=${REQUEST}" -var="threshold_semvar=${THRESHOLD}" > /dev/null ) ; then
        RESULT=$(terraform output is-greater-or-equal)
    else
        RESULT=FAIL
    fi

    if [ $EXPECT != $RESULT ]; then
        echo "  FAIL:    REQUEST[${REQUEST}] to THRESHOLD[${THRESHOLD}] => RESULT[${RESULT}]==EXPECTED[${EXPECT}]"
        SUMMARY=FAIL
    else
        echo "  SUCCESS: REQUEST[${REQUEST}] to THRESHOLD[${THRESHOLD}] => RESULT[${RESULT}]==EXPECTED[${EXPECT}]"
    fi
done
echo "Test results summary: ${SUMMARY}"
if [ $SUMMARY != "SUCCESS" ]; then
    exit 1
fi
exit 0
